use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use r2d2_postgres::{postgres::NoTls, PostgresConnectionManager};
use postgres::config::Config;
use serde::Serialize;
use serde::Deserialize;
use std::path::PathBuf;
use actix_files::NamedFile;



#[derive(Debug, Serialize, Deserialize)]
pub struct Student {
    pub id:String,
    pub fname:String, 
    pub lname:String,
}

impl Student {
    fn new (id:String, fname:String, lname:String) -> Self {
        Student{
            id, fname, lname,
        }
    }
}
impl std::fmt::Display for Student {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> { 
        write!(fmt, "{}, {}, {}", self.id, self.fname, self.lname)
    }
}


#[get("/students")]
async fn get_students(datapool: web::Data<r2d2::Pool<r2d2_postgres::PostgresConnectionManager<postgres::NoTls>>>) -> impl Responder {
    let mut conn = datapool.get().unwrap();
    let students:Vec<Student>;
    if let Ok(rows)  = &conn.query("select * from students", &[]){
       students = rows.into_iter().map(|row| Student::new(row.get(0), row.get(1), row.get(2))).collect::<Vec<Student>>();
        return HttpResponse::Ok().json(students);
    }
    HttpResponse::Ok().body("student")
}

pub enum MyCustomError {
    AppPostgresError, 
    ApplicationError,
}

async fn index() -> actix_web::Result<NamedFile> {
    let path:PathBuf = "./static/index.html".parse().unwrap();
    Ok(NamedFile::open(path)?) 
}

pub fn getconnection() -> Result<r2d2::Pool<r2d2_postgres::PostgresConnectionManager<postgres::NoTls>>, MyCustomError>{
    let mut config = Config::new();
    config.host("localhost");
    config.user("postgres");
    config.dbname("students");
    config.password("myPassword");
    let manager = PostgresConnectionManager::new ( config, NoTls);
    let pool = match r2d2::Pool::builder().build(manager){
        Ok(p) => p,
        _ => return Err(MyCustomError::AppPostgresError),
    };
    Ok(pool)
}

#[actix_web::main]
async fn main() -> std::io::Result<()>{
    let pool = match getconnection() {
        Ok(p) => p,
        _ => panic!("Error in pool")
    };
    HttpServer::new(move || {
        App::new()
        .data(pool.clone())
        .route("/", web::get().to(index))
        .service(get_students)
    }).bind("127.0.0.1:8080")?.run().await

    // if let Ok(rows)  = &conn.query("select * from students", &[]){
    //     for row in rows{
    //         let idstr:&str = row.get(0);
    //         println!("{:?}", idstr);
    //     }
    // }
}